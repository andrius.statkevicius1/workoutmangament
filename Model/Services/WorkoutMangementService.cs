﻿using AutoMapper;
using Domain.DBContexts;
using Domain.Entities.DataModels;
using Domain.Entities.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Model.Services
{
    public class WorkoutMangementService(
        ILogger<WorkoutMangementService> log,
         IMapper mapper,
         WorkoutsDBContext workoutsDBContext
        ) : IWorkoutManagementService
    {
        public async Task<WorkoutDto> CreateWorkoutAsync(WorkoutForCreationDto workoutCreateDto, CancellationToken cancellationToken)
        {
            try
            {
                var workoutEntity = mapper.Map<Workout>(workoutCreateDto);
                await workoutsDBContext.Workouts.AddAsync(workoutEntity, cancellationToken);
                await workoutsDBContext.SaveChangesAsync(cancellationToken);

                return mapper.Map<WorkoutDto>(workoutEntity);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Failed to create workout");
                throw;
            }
        }

        public async Task DeleteWorkoutByIdAsync(int workoutId, CancellationToken cancellationToken)
        {
            try
            {
                var workoutToDelete = await workoutsDBContext.Workouts.SingleOrDefaultAsync(x => x.Id == workoutId, cancellationToken: cancellationToken);
                workoutsDBContext.Workouts.Remove(workoutToDelete!);
                await workoutsDBContext.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Failed to delete workout by id: {WorkoutId}", workoutId);
                throw;
            }
        }

        public async Task<WorkoutDto> GetWorkoutByIdAsync(int workoutId, CancellationToken cancellationToken)
        {
            try
            {
                var workoutEntity = await workoutsDBContext.Workouts.Include(w => w.Excercises).SingleOrDefaultAsync(w => w.Id == workoutId, cancellationToken: cancellationToken);
                return mapper.Map<WorkoutDto>(workoutEntity);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Failed to retrieve workout by id: {WorkoutId}", workoutId);
                throw;
            }
        }

        public async Task<WorkoutByDateDto?> GetWorkoutsByDateAsync(DateOnly date, CancellationToken cancellationToken)
        {
            try
            {
                var workoutsByDate = await workoutsDBContext.WorkoutDates.Include(x => x.Workout).ThenInclude(x => x!.Excercises).Where(x => x.Date == date).ToListAsync(cancellationToken: cancellationToken);

                if (workoutsByDate?.Count == 0)
                    return null;

                return mapper.Map<WorkoutByDateDto>(workoutsByDate);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Failed to retrieve workouts by date: {Date}", date);
                throw;
            }
        }

        public async Task<WorkoutSummaryWithIdDto> GetWorkoutSummaryByIdAsync(int workoutId, CancellationToken cancellationToken)
        {
            try
            {
                var result = await workoutsDBContext.Workouts.Include(x => x.Excercises).SingleOrDefaultAsync(w => w.Id == workoutId, cancellationToken);
                return mapper.Map<WorkoutSummaryWithIdDto>(result);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Failed to retrieve workout by id: {Id}", workoutId);
                throw;
            }
        }

        public async Task<bool> LinkDateToWorkoutAsync(int workoutId, DateOnly date, CancellationToken cancellationToken)
        {
            try
            {
                if (!await WorkoutExistsAsync(workoutId, cancellationToken))
                    return false;

                if (await workoutsDBContext.WorkoutDates.AnyAsync(w => w.Date.Equals(date) && w.WorkoutId == workoutId, cancellationToken: cancellationToken))
                    return true;

                var workoutDate = new WorkoutDate() { Date = date, WorkoutId = workoutId };
                await workoutsDBContext.WorkoutDates.AddAsync(workoutDate, cancellationToken);
                await workoutsDBContext.SaveChangesAsync(cancellationToken);
                return true;
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Failed to link date: {Date} to a workout id: {WorkoutId}", date, workoutId);
                return false;
            }
        }

        public async Task<bool> WorkoutExistsAsync(int workoutId, CancellationToken cancellationToken) => await workoutsDBContext.Workouts.AnyAsync(w => w.Id == workoutId, cancellationToken: cancellationToken);
    }
}
