﻿using Domain.Entities;
using Domain.Entities.DTOs;
using Microsoft.EntityFrameworkCore;

namespace Model.Services
{
    public interface IWorkoutManagementService
    {
        Task<WorkoutDto> CreateWorkoutAsync(WorkoutForCreationDto workoutCreateDto, CancellationToken cancellationToken);
        Task<WorkoutDto> GetWorkoutByIdAsync(int workoutId, CancellationToken cancellationToken);
        Task DeleteWorkoutByIdAsync(int workoutId, CancellationToken cancellationToken);
        Task<bool> WorkoutExistsAsync(int workoutId, CancellationToken cancellationToken);
        Task<bool> LinkDateToWorkoutAsync(int workoutId, DateOnly date, CancellationToken cancellationToken);
        Task<WorkoutByDateDto?> GetWorkoutsByDateAsync(DateOnly dateTime, CancellationToken cancellationToken);
        Task<WorkoutSummaryWithIdDto> GetWorkoutSummaryByIdAsync(int workoutId, CancellationToken cancellationToken);
    }
}