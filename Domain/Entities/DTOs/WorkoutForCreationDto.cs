﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.DTOs
{
    public record WorkoutForCreationDto([Required] string Title, string Description, List<ExerciseForCreationDto> Exercises);
}
