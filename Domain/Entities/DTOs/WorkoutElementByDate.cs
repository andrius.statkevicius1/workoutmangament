﻿namespace Domain.Entities.DTOs
{
    public record WorkoutElementByDate
    {
        public required string Title { get; init; }
        public string? Description { get; init; }
        public WorkoutSummaryWithIdDto? Summary { get; init; }
        public List<ExerciseDto>? Exercises { get; init; }
    }
}