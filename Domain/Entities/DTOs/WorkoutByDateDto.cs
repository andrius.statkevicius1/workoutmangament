﻿namespace Domain.Entities.DTOs
{
    public record WorkoutByDateDto
    {
        public DateOnly Date { get; init; }
        public List<WorkoutElementByDate>? Workouts { get; init; }
    }
}
