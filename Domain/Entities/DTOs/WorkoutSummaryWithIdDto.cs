﻿namespace Domain.Entities.DTOs
{
    public class WorkoutSummaryWithIdDto : WorkoutSummaryDto
    {
        public int WorkoutId { get; init; }
    }
}