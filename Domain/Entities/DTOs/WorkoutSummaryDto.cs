﻿namespace Domain.Entities.DTOs
{
    public class WorkoutSummaryDto
    {
        public int TotalSets { get; init; }
        public int TotalReps { get; init; }
        public int TotalDuration { get; init; }
    }
}