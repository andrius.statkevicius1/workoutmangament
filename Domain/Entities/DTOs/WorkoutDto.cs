﻿namespace Domain.Entities.DTOs
{
    public record WorkoutDto
    {
        public int Id { get; init; }
        public string? Title { get; init; }
        public string? Description { get; init; }
        public ICollection<ExerciseDto>? Exercises { get; init; }

        public WorkoutDto() { }

        public WorkoutDto(int id, string? title, string? description, ICollection<ExerciseDto>? exercises)
        {
            Id = id;
            Title = title;
            Description = description;
            Exercises = exercises;
        }
    }
}
