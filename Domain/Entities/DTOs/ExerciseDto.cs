﻿namespace Domain.Entities.DTOs
{
    public record ExerciseDto
    {
        public string? Name { get; init; }
        public int Sets { get; init; }
        public int Reps { get; init; }
        public string? Duration { get; init; }
    }
}
