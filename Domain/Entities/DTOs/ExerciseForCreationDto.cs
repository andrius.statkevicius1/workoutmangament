﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities.DTOs
{
    public record ExerciseForCreationDto([Required]string Name, int Sets, int Reps, int? Duration);
}
