﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities.DataModels
{
    public class ExerciseItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public required string Name { get; set; }
        // TODO: Not sure what should be mandatory fields when creating a work out and what would be not
        // TODO: Ideally it should be possible to update a certain workout, but this was not in the requirements (hence simplifying)
        public int Sets { get; set; }
        public int Reps { get; set; }
        public int Duration { get; set; }
        public int WorkoutId { get; set; }
        public Workout? Workout { get; set; }
    }
}