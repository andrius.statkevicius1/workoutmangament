﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Domain.Entities.DataModels
{
    public class Workout
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public required string Title { get; set; }
        public string? Description { get; set; }
        [Required]
        public ICollection<ExerciseItem>? Excercises { get; set; }

        public Workout()
        {

        }

        [SetsRequiredMembers]
        public Workout(string title) => Title = title;
    }
}
