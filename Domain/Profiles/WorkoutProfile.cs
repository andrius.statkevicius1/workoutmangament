﻿using AutoMapper;
using Domain.Entities.DataModels;
using Domain.Entities.DTOs;

namespace Domain.Profiles
{
    public class WorkoutProfile : Profile
    {
        public WorkoutProfile()
        {
            CreateMap<WorkoutForCreationDto, Workout>()
                .ForMember(dest => dest.Excercises, opt => opt.MapFrom(src => src.Exercises));
            CreateMap<Workout, WorkoutDto>()
                .ForMember(dest => dest.Exercises, opt => opt.MapFrom(src => src.Excercises));      
            CreateMap<List<WorkoutDate>, WorkoutByDateDto>()
                .ForMember(x => x.Date, opt => opt.MapFrom(src => src.First().Date))
                .ForMember(x => x.Workouts, opt => opt.MapFrom(src => src.Select(wd => wd.Workout)));
            CreateMap<Workout, WorkoutElementByDate>()
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Exercises, opt => opt.MapFrom(src => src.Excercises))
                .ForMember(dest => dest.Summary, opt => opt.MapFrom(src => new WorkoutSummaryWithIdDto
                {
                    TotalSets = src.Excercises != null ? src.Excercises.Sum(e => e.Sets) : 0,
                    TotalReps = src.Excercises != null ? src.Excercises.Sum(e => e.Reps) : 0,
                    TotalDuration = src.Excercises != null ? src.Excercises.Sum(e => e.Duration) : 0
                }));
            CreateMap<Workout, WorkoutSummaryWithIdDto>()
                .ForMember(dest => dest.WorkoutId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TotalReps, opt => opt.MapFrom(src => src.Excercises != null ? src.Excercises.Sum(x => x.Reps) : 0))
                .ForMember(dest => dest.TotalSets, opt => opt.MapFrom(src => src.Excercises != null ? src.Excercises.Sum(x => x.Sets) : 0))
                .ForMember(dest => dest.TotalDuration, opt => opt.MapFrom(src => src.Excercises != null ? src.Excercises.Sum(x => x.Duration) : 0));
        }
    }
}
