﻿using AutoMapper;
using Domain.Entities.DataModels;
using Domain.Entities.DTOs;

namespace Domain.Profiles
{
    public class ExerciseProfile : Profile
    {
        public ExerciseProfile()
        {
            CreateMap<ExerciseForCreationDto, ExerciseItem>();
            CreateMap<ExerciseItem, ExerciseDto>()
                .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => $"{src.Duration} minutes"));
        }
    }
}
