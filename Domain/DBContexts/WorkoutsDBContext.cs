﻿using Domain.Entities.DataModels;
using Microsoft.EntityFrameworkCore;

namespace Domain.DBContexts
{
    public class WorkoutsDBContext(DbContextOptions<WorkoutsDBContext> options) : DbContext(options)
    {
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<ExerciseItem> Exercises { get; set; }
        public DbSet<WorkoutDate> WorkoutDates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Workout>()
                .HasMany(w => w.Excercises)
                .WithOne(e => e.Workout)
                .HasForeignKey(e => e.WorkoutId);

            modelBuilder.Entity<Workout>()
                .HasIndex(w => w.Title)
                .IsUnique();

            modelBuilder.Entity<Workout>()
               .HasIndex(w => w.Title)
               .IsUnique();

            modelBuilder.Entity<ExerciseItem>()
                .HasIndex(w => w.Name)
                .IsUnique();

            modelBuilder.Entity<WorkoutDate>()
                .HasIndex(w => new { w.Date, w.WorkoutId })
                .IsUnique();

            base.OnModelCreating(modelBuilder);
        }
    }
}
