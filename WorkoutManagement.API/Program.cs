using Domain.DBContexts;
using Domain.Entities.DTOs;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;
using Model.Services;
using System.Globalization;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddScoped<IWorkoutManagementService, WorkoutMangementService>();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddDbContext<WorkoutsDBContext>(options => options.UseSqlite(builder.Configuration.GetConnectionString("WorkoutInfoDBConnectionString")));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.MapGet(
    "/workouts/historical/{date}",
    async Task<Results<NotFound<object>, BadRequest<object>, Ok<WorkoutByDateDto>>> (string date, IWorkoutManagementService workoutManagementService, CancellationToken cancellationToken)
    =>
    {
        if (!TryParseDate(date, out DateOnly dateTime))
            return TypedResults.BadRequest<object>(new { Message = "Invalid date format. Please use YYYY-MM-DD" });

        var result = await workoutManagementService.GetWorkoutsByDateAsync(dateTime, cancellationToken);
        return result is null
            ? TypedResults.NotFound<object>(new { Message = $"No workouts have been found with date: {date}" })
            : TypedResults.Ok(result);
    })
    .WithOpenApi()
    .WithName("GetWorkoutSummariesByDate")
    .WithSummary("Provides a summary of workouts for a certain date");

app.MapGet(
    "workouts/{workoutId:int}/summary",
    async Task<Results<NotFound<object>, Ok<WorkoutSummaryWithIdDto>>> (int workoutId, IWorkoutManagementService workoutManagementService, CancellationToken cancellationToken)
    =>
    {
        var result = await workoutManagementService.GetWorkoutSummaryByIdAsync(workoutId, cancellationToken);
        return result is null
            ? TypedResults.NotFound<object>(new { Message = $"Workout with id: {workoutId} not found." })
            : TypedResults.Ok(result);
    });

app.MapGet(
    "/workouts/{workoutId:int}",
    async Task<Results<NotFound<object>, Ok<WorkoutDto>>> (int workoutId, IWorkoutManagementService workoutManagementService, CancellationToken cancellationToken)
    =>
    {
        var result = await workoutManagementService.GetWorkoutByIdAsync(workoutId, cancellationToken);
        return result is null
            ? TypedResults.NotFound<object>(new { Message = $"Workout with id: {workoutId} not found." })
            : TypedResults.Ok(result);
    });

app.MapDelete(
    "/workouts/{workoutId:int}",
    async Task<Results<NotFound<object>, NoContent>> (int workoutId, IWorkoutManagementService workoutManagementService, CancellationToken cancellationToken)
    =>
    {
        if (!await workoutManagementService.WorkoutExistsAsync(workoutId, cancellationToken))
            return TypedResults.NotFound<object>(new { Message = $"Workout with id: {workoutId} not found." });

        await workoutManagementService.DeleteWorkoutByIdAsync(workoutId, cancellationToken);
        return TypedResults.NoContent();
    });

app.MapPost(
    "/workouts/createWorkout",
    async Task<Results<BadRequest, Created<WorkoutDto>>> (IWorkoutManagementService workoutManagementService, WorkoutForCreationDto workoutCreateDto, CancellationToken cancellationToken)
    =>
    {
        var createdItem = await workoutManagementService.CreateWorkoutAsync(workoutCreateDto, cancellationToken);
        var uri = $"/workouts/{createdItem.Id}";
        return TypedResults.Created(uri, createdItem);
    });

app.MapPut(
    "/workouts/{workoutId:int}/date/{dateString}",
    async Task<Results<NotFound<object>, BadRequest<object>, Ok<object>>> (int workoutId, string dateString, IWorkoutManagementService workoutService, CancellationToken cancellationToken) =>
    {
        if (!TryParseDate(dateString, out DateOnly date))
            return TypedResults.BadRequest<object>(new { Message = "Invalid date format. Please use YYYY-MM-DD." });

        bool linked = await workoutService.LinkDateToWorkoutAsync(workoutId, date, cancellationToken);
        if (!linked)
            return TypedResults.NotFound<object>(new { Message = $"Workout with id: {workoutId} and date:{dateString} not found." });

        return TypedResults.Ok<object>(new { Message = "Workout linked successfully", Date = date });
    });

app.UseHttpsRedirection();
app.UseSwagger();
app.UseSwaggerUI();

app.Run();

static bool TryParseDate(string dateString, out DateOnly date)
    => DateOnly.TryParseExact(dateString, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);